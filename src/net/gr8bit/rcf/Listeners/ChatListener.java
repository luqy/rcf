package net.gr8bit.rcf.Listeners;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChatListener implements Listener {
    ArrayList alITALIC = new ArrayList();
    ArrayList alBOLD = new ArrayList();
    ArrayList alULINE = new ArrayList();
    @EventHandler
    public void playerChat(AsyncPlayerChatEvent e) {
        String msg = e.getMessage();

        Pattern pITALIC = Pattern.compile("\\_(.*?)\\_");
        Matcher mITALIC = pITALIC.matcher(msg);



        while(mITALIC.find()) {
            alITALIC.add(mITALIC.group());
        }

        for(String msggood : msg.split(" ")) {
            for(Object mITALICGROUP : alITALIC) {
                if(msggood.contains(mITALICGROUP.toString())) {
                    msg = msg.replace(msggood, ChatColor.ITALIC + ""+mITALICGROUP.toString() + ChatColor.RESET);
                    e.setMessage(msg.replace("_", "").replace("*",""));

                }

            }


        }

        Pattern pBOLD = Pattern.compile("\\*(.*?)\\*");
        Matcher mBOLD = pBOLD.matcher(msg);



        while(mBOLD.find()) {
            alBOLD.add(mBOLD.group());
        }

        for(String msggood : msg.split(" ")) {
            for(Object mBOLDGROUP : alBOLD) {
                if(msggood.contains(mBOLDGROUP.toString())) {
                    msg = msg.replace(msggood, ChatColor.BOLD + ""+mBOLDGROUP.toString() + ChatColor.RESET);
                    e.setMessage(msg.replace("_", "").replace("*",""));

                }

            }


        }

        Pattern pULINE = Pattern.compile("\\.(.*?)\\.");
        Matcher mULINE = pULINE.matcher(msg);



        while(mULINE.find()) {
            alULINE.add(mULINE.group());
        }

        for(String msggood : msg.split(" ")) {
            for(Object mNLINEGROUP : alULINE) {
                if(msggood.contains(mNLINEGROUP.toString())) {
                    msg = msg.replace(msggood, ChatColor.UNDERLINE + ""+mNLINEGROUP.toString() + ChatColor.RESET);
                    e.setMessage(msg.replace("_", "").replace("*","").replace(".",""));

                }

            }


        }




    }
}