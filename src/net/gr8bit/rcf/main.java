package net.gr8bit.rcf;

import net.gr8bit.rcf.Listeners.ChatListener;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Matt on 6/23/16.
 */
public class main extends JavaPlugin {

    @Override
    public void onEnable() {
        //register events
        registerEvents(this, new ChatListener());

    }
    public static void registerEvents(org.bukkit.plugin.Plugin plugin, Listener... listeners) {
        for (Listener listener : listeners) {
            Bukkit.getServer().getPluginManager().registerEvents(listener, plugin);
        }
    }
}